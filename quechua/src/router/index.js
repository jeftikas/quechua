import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/diccionario',
    name: 'diccionario',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Diccionary.vue')
  },
  {
    path: '/conjugador',
    name: 'conjugador',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Conjugador.vue')
  },
  {
    path: '/sufijos',
    name: 'sufijos',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Sufijos.vue')
  },
  {
    path: '/traductor',
    name: 'traductor',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Traductor.vue')
  },
  {
    path: '/gramatica',
    name: 'gramatica',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Gramatica.vue')
  },
  {
    path: '/palabra',
    name: 'palabra',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazpalabray-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Palabra.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
