const admin = require('firebase-admin');
const serviceAccount = require("./key_service_account.json");
const data = require("./nuveodicc.json");
const collectionKey = "words"; //Name of the collection
const parse =  Object.assign({}, data)
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});
const firestore = admin.firestore();
const settings = {timestampsInSnapshots: true};
firestore.settings(settings);
if (parse && (typeof parse === "object")) {
Object.keys(parse).forEach(docKey => {
 firestore.collection(collectionKey).doc(docKey).set(parse[docKey]).then((res) => {
    console.log("Document " + docKey + " successfully written!");
}).catch((error) => {
   console.error("Error writing document: ", error);
});
});
}